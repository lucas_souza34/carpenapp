import React, { Component } from "react";
import web3 from "../web3";
import abi from "../abi";
import bytecode from "../bytecode";

export default class Intermediador extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nome: "",
      simbolo: "",
      valor: "",
      intermediador: ""
    };
    
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    this.setState({[event.target.name]: event.target.value});
  }

  async handleSubmit(event){
    event.preventDefault();

    const valorWei = web3.utils.toWei(this.state.valor, "ether");
    const accounts = await web3.eth.getAccounts();
    let contrato = await new web3.eth.Contract(abi, null, {data : bytecode});
    await contrato.deploy({
      data: bytecode,
      arguments: [this.state.nome, this.state.simbolo, this.state.intermediador, valorWei]
    }).send({
      from: accounts[0]
    }).then((instance) => {
        alert("Contract deployed on address: " + instance.options.address);
    });
  }

  render() {
    return (
      <div id="content">
        <h1>Cadastrar</h1>
        <form onSubmit={this.handleSubmit}>
          <label>
            Nome:
          </label>
            <input name="nome" type="text" placeholder="Insira o nome do produto" value={this.state.nome} onChange={this.handleInputChange}/>
          <label>
            Símbolo:
            <input name="simbolo" type="text" placeholder="Insira o simbolo dos tokens" value={this.state.simbolo} onChange={this.handleInputChange}/>
          </label>
          <label>
            Valor em ETH:
            <input name="valor" type="number" placeholder="Insira o valor do produto em Ether" value={this.state.valor} onChange={this.handleInputChange}/>
          </label>
          <label>
            Endereço Intermediador:
            <input name="intermediador" type="text" placeholder="Insira o endereço do intermediador" value={this.state.intermediador} onChange={this.handleInputChange}/>
          </label>
          <input id="submit-form" type="submit" value="Criar Contrato" />
        </form>
      </div>
    );
  }
}