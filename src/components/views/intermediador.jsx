import React, { Component } from "react";
import web3 from "../web3";
import abi from "../abi";

export default class Intermediador extends Component {
  constructor(props) {
    super(props);
    this.state = {
      produto: "",
      simbolo: "",
      valor: "",
      status: "",
			vendedor: "",
			comprador: "",
			contrato: "",
			accounts: ""
    };
  }

  async componentDidMount(){
		const account = await web3.eth.getAccounts();
		this.setState({contrato: new web3.eth.Contract(abi, this.props.location.query.produto)});

		const produto = await this.state.contrato.methods.name().call();
		const simbolo = await this.state.contrato.methods.symbol().call();
		const status = await this.state.contrato.methods.getStatus().call();
    const valor = await this.state.contrato.methods.valorCustodia().call();
		const	vendedor = await this.state.contrato.methods.vendedor().call();
    const comprador = await this.state.contrato.methods.comprador().call();
		this.setState({produto, simbolo, status, comprador, vendedor, account, valor});
  }


  render() {
    return (
      <div id="content">
        <div id="titulo"><h1>Produto</h1></div>
        <div className="dados">Produto:
          <a href={"https://ropsten.etherscan.io/address/" + this.props.location.query.produto} target="_blank"><span>{this.state.produto}</span></a>
        </div>
        <div className="dados">Valor:
          <span>{web3.utils.fromWei(this.state.valor, "ether") + " ETH"}</span>
        </div>
        <div className="dados">Vendedor:
          <a href={"https://ropsten.etherscan.io/address/" + this.state.vendedor} target="_blank"><span>{this.state.vendedor}</span></a>
        </div>
        <div className="dados">Comprador:
          <a href={"https://ropsten.etherscan.io/address/" + this.state.comprador} target="_blank"><span>{this.state.comprador}</span></a>
        </div>
        <div id="botoes">
          <button className="button">Aprovar</button>
          <button className="button">Recusar</button>
        </div>
        <h2>Status</h2>
        <hr></hr>
        <ul>
          <li><p className="status">{this.state.status}</p></li>
        </ul>
    	</div>
    );
  }
}