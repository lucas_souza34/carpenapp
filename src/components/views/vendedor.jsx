import React, { Component } from "react";
import { browserHistory } from 'react-router';
import web3 from "../web3";
import abi from "../abi";

export default class Vendedor extends Component {
	constructor(props) {
    super(props);
    this.state = {
      produto: "",
      simbolo: "",
      valor: "",
      status: "",
	    intermediador: "",
      comprador: "",
      saldo: "",
      contrato: "",
      accounts: ""
		};
	}

	async componentDidMount(){
		let comprador;
		const account = await web3.eth.getAccounts();
		this.setState({contrato: new web3.eth.Contract(abi, this.props.location.query.produto)});

		const produto = await this.state.contrato.methods.name().call();
		const simbolo = await this.state.contrato.methods.symbol().call();
    const status = await this.state.contrato.methods.getStatus().call();
    const valor = await this.state.contrato.methods.valorCustodia().call();
		if(status == "Aguardando Pagamento"){
			comprador = "Aguardando Comprador";
		} else {
			comprador = await this.state.contrato.methods.comprador().call();
		}
		const intermediador = await this.state.contrato.methods.intermediador().call();
		const saldo = await this.state.contrato.methods.balanceOf(account[0]).call();
		this.setState({produto, simbolo, status, intermediador, comprador, saldo, account, valor});
	}

  render() {
    return (
    	<div id="content">
				<div id="titulo"><h1>Produto</h1></div>
				<div className="dados">Produto:
					<a href={"https://ropsten.etherscan.io/address/" + this.props.location.query.produto} target="_blank"><span>{this.state.produto}</span></a>
				</div>
        <div className="dados">Valor do Produto:
					<span>{web3.utils.fromWei(this.state.valor, "ether") + " ETH"}</span>
				</div>
				<div className="dados">Saldo:
					<span>{web3.utils.fromWei(this.state.saldo, "ether")} {this.state.simbolo}</span>
				</div>
				<div className="dados">Intermediador:
					<a href={"https://ropsten.etherscan.io/address/" + this.state.comprador} target="_blank"><span>{this.state.comprador}</span></a>
				</div>
				<div className="dados">Comprador:
					<a href={"https://ropsten.etherscan.io/address/" + this.state.comprador} target="_blank"><span>{this.state.comprador}</span></a>
				</div>
				{this.state.status == "Acordo em Vigência" &&
					<div id="botoes">
						<button className="button">Solicitar Liberação</button>
						<button className="button">Aprovar Devolução</button>
					</div>
				}
				<h2>Status</h2>
				<hr></hr>
				<ul>
					<li><p className="status">{this.state.status}</p></li>
				</ul>
			</div>
    );
  }
}