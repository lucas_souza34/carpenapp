import React, { Component } from "react";
import web3 from "../web3";
import abi from "../abi";

export default class Comprador extends Component {
  constructor(props) {
    super(props);
    this.state = {
      produto: "",
      simbolo: "",
      valor: "",
      status: "",
			intermediador: "",
			vendedor: "",
			contrato: "",
			accounts: ""
    };
    
    this.efetuarPagamento = this.efetuarPagamento.bind(this);
    this.aprovaPagamento = this.aprovaPagamento.bind(this);
    this.solicitaDevolucao = this.solicitaDevolucao.bind(this);
  }
  
  async componentDidMount(){
		const account = await web3.eth.getAccounts();
		this.setState({contrato: new web3.eth.Contract(abi, this.props.location.query.produto)});

		const produto = await this.state.contrato.methods.name().call();
		const simbolo = await this.state.contrato.methods.symbol().call();
		const status = await this.state.contrato.methods.getStatus().call();
    const valor = await this.state.contrato.methods.valorCustodia().call();
		const	vendedor = await this.state.contrato.methods.vendedor().call();
    const intermediador = await this.state.contrato.methods.intermediador().call();
		this.setState({produto, simbolo, status, intermediador, vendedor, account, valor});
  }

  async efetuarPagamento(event){
    event.preventDefault();
    await this.state.contrato.methods.depositaPagamento().send({from: this.state.account[0], value: this.state.valor});
  }

  async aprovaPagamento(event){
    event.preventDefault();
    await this.state.contrato.methods.aprovaPagamento(true).send({from: this.state.account[0]});
  }

  async solicitaDevolucao(event){
    event.preventDefault();
    await this.state.contrato.methods.solicitaDevolucao().send({from: this.state.account[0]});
  }

  render() {
    let button;

    if(this.state.status == "Aguardando Pagamento"){
      button = 
        <div id="botoes">
          <button className="button" onClick={this.efetuarPagamento}>Efetuar Pagamento</button>
        </div>
      ;
    } else {
      button = 
        <div id="botoes">
          <button className="button" onClick={this.solicitaDevolucao}>Solicitar Devolução</button>
          <button className="button" onClick={this.aprovaPagamento}>Aprovar Pagamento</button>
        </div>
      ;
    }
    return (
      <div id="content">
        <div id="titulo"><h1>Produto</h1></div>
        <div className="dados">Produto:
          <a href={"https://ropsten.etherscan.io/address/" + this.props.location.query.produto} target="_blank"><span>{this.state.produto}</span></a>
        </div>
        <div className="dados">Valor:
          <span>{web3.utils.fromWei(this.state.valor, "ether") + " ETH"}</span>
        </div>
        <div className="dados">Vendedor:
          <a href={"https://ropsten.etherscan.io/address/" + this.state.vendedor} target="_blank"><span>{this.state.vendedor}</span></a>
        </div>
        <div className="dados">Intermediador:
          <a href={"https://ropsten.etherscan.io/address/" + this.state.intermediador} target="_blank"><span>{this.state.intermediador}</span></a>
        </div>
        {button}
        <h2>Status</h2>
        <hr></hr>
        <ul>
          <li><p className="status">{this.state.status}</p></li>
        </ul>
    	</div>
    );
  }
}