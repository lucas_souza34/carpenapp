import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/app';
import Vendedor from './components/views/vendedor';
import Comprador from './components/views/comprador';
import Intermediador from './components/views/intermediador';
import Cadastrar from './components/views/cadastrar';

export default (
  <Route path='/' component={App}>
    <IndexRoute component={Cadastrar} />
    <Route path='comprador' component={Comprador} />
    <Route path='vendedor' component={Vendedor} />
    <Route path='intermediador' component={Intermediador} />
    <Route path='cadastrar' component={Cadastrar} />
    <Route path='*' component={Cadastrar} />
  </Route>
);